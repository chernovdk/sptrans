import Vue from 'vue';
import Router from 'vue-router';
import MainView from './views/MainView.vue';
import StepOne from './views/StepOne.vue';
import StepTwo from './views/StepTwo.vue';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'MainView',
      component: MainView,
    },
    {
      path: '/step1',
      name: 'StepOne',
      component: StepOne,
    },
    {
      path: '/step2',
      name: 'StepTwo',
      component: StepTwo,
    },
  ],
});
